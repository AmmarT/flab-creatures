import javax.swing.JOptionPane;

import hts.aiproject.*;
import hts.aiproject.graphics.*;
import hts.aiproject.graphics.scenes.*;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.MathUtils;

public class LoadingScene extends Scene {
	
	//Stores the rendering object for rendering everything to the scene.
	private SpriteBatch batch;
	//Stores the loading scene's text renderer.
	private BitmapFont text;
	
	//Stores a flag, determines whether the game has been loaded or not.
	private boolean loaded;
	//Stores flags for whether the saves have been loaded.
	private boolean savesLoaded;
	//Stores the current number of frames passed whilst the loading scene is active.
	private int frameCount;
	//Stores the max font size that has been loaded so far.
	private int maxFont;

	//Constructor for the loading scene scene.
	//Loads in any necessary assets to render the loading scene.
	public LoadingScene(Core core, Object[] parameters) {
		super(core, "LoadingScene");
		
		//Initialises the objects used for rendering the loading scene.
		batch = new SpriteBatch();
		text = new BitmapFont();
	}

	//Renders the loading scene.
	@Override
	public void draw() {
		super.draw();
		//Stores the title text.
		String title = "FLab Creatures";
		
		//Begins rendering.
		batch.begin();
		//Renders the loading scene text in the top left corner
		text.draw(batch, "Loading...", 8, Gdx.graphics.getHeight() - 8);
		
		//Only renders the logo if the max font size is greater than 0.
		for(int size = 1; size <= maxFont && maxFont > 1; size++) {
			//Gets the font.
			BitmapFont font = core.getTextRenderer().getSize(size);
			//Renders the logo text in the centre of the scene with the current largest loaded font size.
			font.draw(batch, title,
					(getWidth() - TextRenderer.getTextWidth(font, title)) / 2,
					(getHeight() - TextRenderer.getTextHeight(font, title)) / 2);
		}
		
		//Renders the current post-text load operation to below the title.
		if(maxFont == TextRenderer.MAX_SIZE) {
			//Gets the font to render the sub-title in.
			BitmapFont font = core.getTextRenderer().getSize(maxFont / 4);
			//Gets the max size font.
			BitmapFont maxSizeFont = core.getTextRenderer().getSize(maxFont);
			
			/*if(!savesLoaded) {
				font.draw(batch, "Loading saves...",
						(getWidth() - TextRenderer.getTextWidth(font, "Loading saves...")) / 2,
						((getHeight() - TextRenderer.getTextHeight(maxSizeFont, title)) / 2)
							- (TextRenderer.getTextHeight(maxSizeFont, "Loading saves...") * 1.5f));
			}*/
		}
		
		//Ends rendering.
		batch.end();
	}

	//Handles any logic for the loading scene.
	@Override
	public void tick() {
		//Checks if the frame count is high enough to load whilst displaying the splash scene.
		if(frameCount >= 1) {
			//Checks if the game has been loaded.
			if(loaded && maxFont == TextRenderer.MAX_SIZE) {
				//Loads all of the game's saves.
				if(!savesLoaded) {
					//core.loadSaves();
					savesLoaded = true;
					return;
				}
				
				//Plays a loading sound effect.
				core.getSoundManager().playSoundEffect("load");
				//Sets the scene to the menu scene.
				core.setScene("MainMenu");
				//JOptionPane.showMessageDialog(null, "Pause"); //Pause for debugging
			} else { //If it hasn't been loaded, loads the assets for the game. TBI
				//Runs the load function.
				if(!loaded)
					core.getTextRenderer().load("assets/fonts/standard.ttf");
				//Sets the flag storing whether the assets have been loaded to true.
				loaded = true;
				
				//Generates fonts for each size.
				if(maxFont < TextRenderer.MAX_SIZE) {
					//Generates the next font size.
					core.getTextRenderer().generateFont(maxFont);
					//Updates the font size counter.
					maxFont++;
				}
			}
		} else {
			//Loads the cursor.
			Gdx.graphics.setCursor(core.getTextureManager().getCursor("assets/textures/cursors/standard.png", 0, 0));
		}
		
		frameCount++;
	}

	//Disposes the assets used to render the loading scene.
	@Override
	public void dispose() {
		super.dispose();
		batch.dispose();
		text.dispose();
	}
}