import java.io.IOException;

import hts.aiproject.*;
import hts.aiproject.graphics.*;
import hts.aiproject.graphics.scenes.*;
import hts.aiproject.utils.maths.HTSMaths;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.badlogic.gdx.utils.Align;

import com.kotcrab.vis.ui.building.utilities.Alignment;
import com.kotcrab.vis.ui.util.InputValidator;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisSlider;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTextField;
import com.kotcrab.vis.ui.widget.VisTextField.TextFieldListener;
import com.kotcrab.vis.ui.widget.VisValidatableTextField;
import com.kotcrab.vis.ui.widget.VisWindow;

public class MainMenu extends Scene {
	
	//Stores the rendering object for rendering everything to the scene.
	private SpriteBatch batch;
	
	//Stores the buttons to start, exit, and go to the settings menu.
	private VisTextButton start, settings, exit;
	//Stores the character selection and settings windows.
	private final VisWindow characterSelectionWindow, settingsWindow;

	//Constructor for the menu scene.
	//Loads in any necessary assets to render the menu scene.
	public MainMenu(final Core core, Object[] parameters) {
		super(core, "MainMenu");
		
		//Initialises the objects used for rendering the menu scene.
		batch = new SpriteBatch();
		//Loads the character selection window.
		//characterSelectionWindow = (VisWindow) core.getScriptManager().run("sub-scenes/load-character-selection", core);
		//Loads the settings window.
		settingsWindow = new VisWindow("Audio Settings");
		settingsWindow.add((VisTable) core.getScriptManager().run("sub-scenes/load-audio-settings", core));
		settingsWindow.setTransform(true);
		settingsWindow.setScale(2);
		settingsWindow.pack();
		settingsWindow.setPosition((Gdx.graphics.getWidth() - settingsWindow.getWidth() * 2) / 2,
										(Gdx.graphics.getHeight() - settingsWindow.getHeight() * 2) / 2);
		settingsWindow.addCloseButton();
		
		//Initialises the buttons and their associated logic.
		start = new VisTextButton("Start", new ChangeListener() {
			@Override
			public void changed(ChangeEvent e, Actor actor) {
				
			}
		});
		
		settings = new VisTextButton("Settings", new ChangeListener() {
			@Override
			public void changed(ChangeEvent e, Actor actor) {
				actor.getStage().addActor(settingsWindow);
			}
		});
		
		exit = new VisTextButton("Quit", new ChangeListener() {
			@Override
			public void changed(ChangeEvent e, Actor actor) {
				Gdx.app.exit();
			}
		});
		
		start.setFocusBorderEnabled(false);
		settings.setFocusBorderEnabled(false);
		exit.setFocusBorderEnabled(false);
		
		start.setTransform(true);
		settings.setTransform(true);
		exit.setTransform(true);
		start.setScale(4);
		settings.setScale(4);
		exit.setScale(4);
		
		start.setPosition((getWidth() - start.getWidth() * 4) / 2,
				getHeight() * 0.66f);
		settings.setPosition((getWidth() - settings.getWidth() * 4) / 2,
				start.getY() - (3 * (start.getHeight() + settings.getHeight())));
		exit.setPosition((getWidth() - exit.getWidth() * 4) / 2,
				settings.getY() - (3 * (settings.getHeight() + exit.getHeight())));
		
		this.addActor(start);
		this.addActor(settings);
		this.addActor(exit);
	}

	//Renders the menu scene.
	@Override
	public void draw() {
		//Gets the title screen font.
		BitmapFont titleFont = core.getTextRenderer().getSize(TextRenderer.MAX_SIZE);
		
		//Draws the title at the top of the window.
		batch.begin();
		titleFont.draw(batch, "FLab Creatures",
				(getWidth() - TextRenderer.getTextWidth(titleFont, "FLab Creatures")) / 2,
				getHeight() - 8);
		batch.end();
		
		//Plays the menu music.
		core.getSoundManager().playMusic("menu");
		//Draws the stage.
		super.draw();
	}

	//Handles any logic for the menu scene.
	@Override
	public void tick() {
		super.act();
	}

	//Disposes the assets used to render the loading scene.
	@Override
	public void dispose() {
		super.dispose();
		batch.dispose();
	}
}