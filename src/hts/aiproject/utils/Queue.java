package hts.aiproject.utils;

import java.util.Vector;

public class Queue<T> extends Vector<T> {
	private static final long serialVersionUID = 1L;
	
	/**Adds a value to the end of the queue.*/
	public void enqueue(T value) {
		/**Simply adds the value, the vector class assumes that the last value in should go to the end, similar to a stack.*/
		add(value);
	}
	
	/**Returns the next value in the queue without removing it.*/
	public T peek() {
		/**The next value to be returned is the first value in the list, hence it is at index 0.*/
		return get(0);
	}
	
	/**Removes the first item from the queue and returns the value.*/
	public T dequeue() {
		/**Returns the next value in the vector by removing the first value and returning the output of the function.*/
		return remove(0);
	}
	
	/**Checks if the queue is populated or not.*/
	public boolean isEmpty() {
		/**If the queue's underlying vector has a size of 0, the queue is considered empty.*/
		return size() == 0;
	}
}