package hts.aiproject.utils.maths.ai;

import hts.aiproject.utils.maths.Matrix;

public class NeuralNetworkLayer {
	
	//Stores the matrix of the weights between the nodes of this layer and the next.
	//Stores the vector of the current values of the vector and the biases for the computation.
	public Matrix weights, bias, values;

	//Takes in the number of nodes in the current layer, along with the next layer.
	public NeuralNetworkLayer(int nodes, int nextNodes) {
		this.weights = new Matrix(nextNodes, nodes);
		this.bias = new Matrix(nextNodes, 1);
		this.values = new Matrix(nodes, 1);
	}
	
	//Randomises the weights and biases.
	public void randomise() {
		for(int i = 0; i < weights.rows; i++)
			for(int j = 0; j < weights.columns; j++)
				weights.matrix[i][j] = Math.random() * (Math.random() < 0.5 ? -1 : 1);
		for(int i = 0; i < bias.rows; i++)
			bias.matrix[i][0] = Math.random() * (Math.random() < 0.5 ? -1 : 1);
	}
	
	//Takes the next layer and passes through the weighted sum for each of the nodes.
	public NeuralNetworkLayer pass(NeuralNetworkLayer next) {
		//Calculates the values for the next layer of the node and sets them to the correct values.
		next.values = weights.mult(values).add(bias).runElementWiseOperation((k) -> 1 / (1 + Math.pow(Math.E, -k)));
		return next;
	}
}