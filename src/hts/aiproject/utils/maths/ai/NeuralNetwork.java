package hts.aiproject.utils.maths.ai;

public class NeuralNetwork {
	
	//Stores the array of the node layers within the network.
	//The first layer acts as an input layer, the last acts as an output layer.
	public NeuralNetworkLayer[] layers;
	
	//Takes in the nodes per layer for each layer of the network.
	public NeuralNetwork(boolean randomise, int... nodesPerLayer) {
		layers = new NeuralNetworkLayer[nodesPerLayer.length];
		
		if(randomise)
			for(NeuralNetworkLayer layer : layers)
				layer.randomise();
	}
	
	//Takes in a pre-existing set of layers
	public NeuralNetwork(NeuralNetworkLayer... layers) {
		this.layers = layers;
	}
	
	//Passes through the network given a set of initial values.
	public void pass(double... input) {
		//Sets the first layer's value vector equal to the values in the input.
		for(int i = 0; i < layers[0].values.matrix.length; i++)
			layers[0].values.matrix[i][0] = input[i];
		//Passes through every layer besides the last.
		for(int i = 0; i < layers.length - 2; i++)
			layers[i + 1] = layers[i].pass(layers[i + 1]);
	}
}