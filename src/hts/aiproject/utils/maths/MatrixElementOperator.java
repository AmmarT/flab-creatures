package hts.aiproject.utils.maths;

public interface MatrixElementOperator {
	double process(double d);
}