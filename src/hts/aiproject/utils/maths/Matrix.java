package hts.aiproject.utils.maths;

public class Matrix {
	
	//Stores the number of rows and columns in the matrix.
	public int rows, columns;
	//Stores the values within the matrix.
	public double[][] matrix;
	
	//Creates a matrix given the rows and columns.
	public Matrix(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		this.matrix = new double[rows][columns];
	}
	
	//Multiplies the elements by a scalar value.
	public Matrix scale(double scalar) {
		//Creates a matrix to store the result.
		Matrix result = new Matrix(rows, columns);
		result.matrix = matrix;
		for(int i = 0; i < rows; i++)
			for(int j = 0; j < columns; j++)
				result.matrix[i][j] *= scalar;
		return result;
	}
	
	//Divides by multiplying by the reciprocal of the scalar.
	public Matrix div(double scalar) {
		//Returns the result by multiplying by the reciprocal of the scalar.
		return scale(1 / scalar);
	}
	
	//Adds two matrices together, they must be the same size.
	public Matrix add(Matrix m) {
		//Checks if the matrices are the same size.
		if(m.rows == rows && m.columns == columns) {
			//Creates a new matrix to store the result.
			Matrix result = new Matrix(rows, columns);
			//Iterates through the rows and columns and adds the elements at the positions to each other.
			for(int i = 0; i < result.rows; i++)
				for(int j = 0; j < result.columns; j++)
					result.matrix[i][j] = matrix[i][j] + m.matrix[i][j];
			return result;
		} else return null;
	}
	
	//Subtracts two matrices, multiplies the matrix by -1 before adding.
	public Matrix sub(Matrix m) {
		return add(m.scale(-1));
	}
	
	//Multiplies two matrices together.
	public Matrix mult(Matrix m) {
		//Matrix to store the result.
		Matrix result = new Matrix(rows, m.columns);
		
		for(int i = 0; i < result.rows; i++) {
			for(int j = 0; j < result.columns; j++) {
				//Loops through each of the columns in the row in the first matrix.
				//Loops through each of the rows in the column in the second matrix.
				//Finds the dot product of these two vectors.
				double dot = 0;
				//Calculates the dot product.
				for(int k = 0; k < columns; k++)
					dot += matrix[i][k] * m.matrix[k][j];
				//Sets the spot in the resultant matrix to this dot product.
				result.matrix[i][j] = dot;
			}
		}
		
		return result;
	}
	
	//Applies the sigmoid function on to each of the elements of the matrix.
	public Matrix runElementWiseOperation(MatrixElementOperator o) {
		//Matrix to store the result.
		Matrix result = new Matrix(rows, columns);
		for(int i = 0; i < result.rows; i++)
			for(int j = 0; j < result.columns; j++)
				result.matrix[i][j] = o.process(matrix[i][j]);
		return result;
	}
}