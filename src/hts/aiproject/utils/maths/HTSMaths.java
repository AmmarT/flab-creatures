package hts.aiproject.utils.maths;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


public class HTSMaths {
	
	/*Credit to Riven: http://www.java-gaming.org/topics/extremely-fast-sine-cosine/36469/view.html*/
	private static final int SIN_BITS, SIN_MASK, SIN_COUNT;
    private static final float radFull, radToIndex;
    private static final float degFull, degToIndex;
    private static final float[] sin, cos;

    static {
        SIN_BITS = 12;
        SIN_MASK = ~(-1 << SIN_BITS);
        SIN_COUNT = SIN_MASK + 1;

        radFull = (float) (Math.PI * 2.0);
        degFull = (float) (360.0);
        radToIndex = SIN_COUNT / radFull;
        degToIndex = SIN_COUNT / degFull;

        sin = new float[SIN_COUNT];
        cos = new float[SIN_COUNT];

        for (int i = 0; i < SIN_COUNT; i++) {
            sin[i] = (float) Math.sin((i + 0.5f) / SIN_COUNT * radFull);
            cos[i] = (float) Math.cos((i + 0.5f) / SIN_COUNT * radFull);
        }

        // Four cardinal directions (credits: Nate)
        for (int i = 0; i < 360; i += 90) {
            sin[(int) (i * degToIndex) & SIN_MASK] = (float) Math.sin(i * Math.PI / 180.0);
            cos[(int) (i * degToIndex) & SIN_MASK] = (float) Math.cos(i * Math.PI / 180.0);
        }
    }

    public static final float sin(float rad) {
        return sin[(int) (rad * radToIndex) & SIN_MASK];
    }

    public static final float cos(float rad) {
        return cos[(int) (rad * radToIndex) & SIN_MASK];
    }
	
	/**Rounds a given number to a given number of decimal places.*/
	public static float roundTo(float number, int decimalPlaces) {
		/**Calculates the exponent to 10 of the decimal places.
		 * Shorthanded from here-on as N.*/
		float N = (float) Math.pow(10, decimalPlaces);
		/**Multiplies the number by N.
		 * Rounds the number.
		 * Divides the number by N.
		 * Returns the result.*/
		return (float) Math.round(number * N) / N;
	}
	
	/**Outputs a random Gaussian distributed number.*/
	public static float randomGaussian() {
		return (float) new Random().nextGaussian();
	}
	
	/**Converts an {@link java.util.ArrayList} to an array of primitive int types.*/
	public static int[] toIntArray(List<Integer> intList) {
		int[] array = new int[intList.size()];
		Iterator<Integer> iterator = intList.iterator();
		for(int i = 0; i < array.length; i++)
			array[i] = iterator.next().intValue();
		return array;
	}
	
	/**Finds the index of the maximum element in a given array of integers.*/
	public static int maxIndex(int[] array) {
		int maxI = 0;
		for(int i = 1; i < array.length; i++)
			if(array[i] > array[maxI])
				maxI = i;
		return maxI;
	}
	
	/**Finds the index of the minimum element in a given array of integers.*/
	public static int minIndex(int[] array) {
		int minI = 0;
		for(int i = 1; i < array.length; i++)
			if(array[i] < array[minI])
				minI = i;
		return minI;
	}
	
	/**Finds the index of the minimum non-zero element in a given array of integers.*/
	public static int minNonZeroIndex(int[] array) {
		int minI = 0;
		
		for(int i = 0; i < array.length; i++) {
			if(array[i] == 0) {
				continue;
			} else if(array[i] < array[minI] || array[minI] == 0) {
				minI = i;
			}
		}
		
		return minI;
	}
	
	/**Flattens a 2D "matrix"-array into a 1D "vector"-array.
	 * @param <T>*/
	@SuppressWarnings("unchecked")
	public static <T> T[] flattenMatrix(T[][] matrix) {
		if(matrix.length > 0) {
			Object[] vector = new Object[matrix.length * matrix[0].length];
			int vectorStep = 0;
			
			for(int i = 0; i < matrix.length; i++) {
				for(int j = 0; j < matrix[i].length; j++) {
					vector[vectorStep++] = matrix[i][j];
				}
			}
			
			return (T[]) vector;
		} else return (T[]) new Object[0];
	}
	
	/**Trims the dimension of a 1D "vector"-array.
	 * @param T*/
	@SuppressWarnings("unchecked")
	public static <T> T[] trimDimension(int newDimension, T... vector) {
		if(newDimension < vector.length) {
			Object[] trimmed = new Object[newDimension];
			for(int i = 0; i < newDimension; i++)
				trimmed[i] = vector[i];
			return (T[]) trimmed;
		} else return vector;
	}

    /**Takes in HashMap of floats and T, and returns a sorted Vector of T in descending order
      * 
    */
    public static Vector<T> sortDesc(HashMap<Float, T>) {
        Map<Float, T> sorted = new TreeMap<Float, T>(
            new Comparator<Float>() {
                @Override
                public int compare(float a, float b){
                    return b.compareTo(a);
                }
            });

        return new Vector<T>(sorted.values());
    }
}