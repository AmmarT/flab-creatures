package hts.aiproject.utils;

import hts.aiproject.Core;

public interface Loadable {
	void load(Core core, Object... parameters);
}