package hts.aiproject.utils;

import java.util.Comparator;

public class PriorityQueue<T> extends Queue<T> {
	private static final long serialVersionUID = 1L;
	
	/**Stores the comparator for the queue.*/
	protected Comparator<T> comparator;
	
	/**Constructor for the priority queue, takes in the method of sorting for the queue.*/
	public PriorityQueue(Comparator<T> comparator) {
		this.comparator = comparator;
	}
	
	/**Overrides the enqueue method to sort upon addition.*/
	@Override
	public void enqueue(T value) {
		super.enqueue(value);
		/**Performs a sort upon enqueueing the value.*/
		sort(comparator);
	}
	
	/**Overrides the dequeue method to sort upon removal.*/
	@Override
	public T dequeue() {
		T value = super.dequeue();
		/**Performs a sort upon dequeueing the value.*/
		sort(comparator);
		/**Returns the value.*/
		return value;
	}
}