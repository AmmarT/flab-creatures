package hts.aiproject.utils.input;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class InputManager {
	
	private HashMap<String, Integer> stringKeys;
	private int[] clickDuration;
	private int[] keyPressDuration;
	private long[] lastPressTime;
	private boolean[] doublePressed;
	
	public InputManager() {
		stringKeys = new HashMap<String, Integer>();
		clickDuration = new int[16];
		keyPressDuration = new int[256];
		lastPressTime = new long[256];
		doublePressed = new boolean[256];
		
		for(int key = 0; key < keyPressDuration.length; key++) {
			String value = Keys.toString(key);
			if(value != null && !value.equals("?"))
				stringKeys.put(value, key);
		}
	}
	
	public void tick() {
		/**Tests mouse clicks for mouse buttons.*/
		for(int button = 0; button < 16; button++) {
			clickDuration[button] = (Gdx.input.isButtonPressed(button)) ? clickDuration[button] + 1 : 0;
		}
		
		/**Tests key presses for all keys to check for.*/
		for(int key = 0; key < keyPressDuration.length; key++) {
			if(Gdx.input.isKeyPressed(key)) {
				keyPressDuration[key]++;
				
				/**Checks if key just pressed.*/
				if(keyPressDuration[key] == 1) {
					/**Gets current time in milliseconds.*/
					long now = System.currentTimeMillis();
					/**Checks if difference between last press times is less than 0.15 seconds.
					 * If so, registers a double press.*/
					doublePressed[key] = now - lastPressTime[key] <= 150;
					lastPressTime[key] = now;
				} else {
					doublePressed[key] = false;
				}
			} else {
				keyPressDuration[key] = 0;
			}
		}
	}
	
	public boolean clicked(int button) {
		return clickDuration[button] > 0;
	}

	public boolean justClicked(int button) {
		return clickDuration[button] == 1;
	}
	
	public boolean keyPressed(int key) {
		return keyPressDuration[key] > 0;
	}
	
	public boolean keyJustPressed(int key) {
		return keyPressDuration[key] == 1;
	}
	
	public boolean keyDoublePressed(int key) {
		return doublePressed[key];
	}
	
	public int keyFromString(String key) {
		return stringKeys.get(key);
	}
	
	public int getMouseX() {
		return Gdx.input.getX();
	}
	
	public int getMouseY() {
		return Gdx.graphics.getHeight() - Gdx.input.getY();
	}
	
	public ArrayList<String> getKeysPressed() {
		ArrayList<String> keysPressed = new ArrayList<String>();
		
		for(String key : stringKeys.keySet())
			if(Gdx.input.isKeyJustPressed(stringKeys.get(key)))
				if(key.length() <= 1 || key.equals("Space") || key.equals("Delete"))
					keysPressed.add(key);
		return keysPressed;
	}
	
	public int[] getClickDuration() {
		return clickDuration;
	}
	
	public int[] getKeyPressDuration() {
		return keyPressDuration;
	}
}