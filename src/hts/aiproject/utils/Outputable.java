package hts.aiproject.utils;

public interface Outputable<T> {
	/**Outputs a value from this function with the generic type specified.*/
	T getOutput();
}