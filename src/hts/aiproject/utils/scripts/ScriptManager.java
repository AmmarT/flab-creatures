package hts.aiproject.utils.scripts;

import java.util.concurrent.ConcurrentHashMap;

import hts.aiproject.Core;
import hts.aiproject.file.FileHandler;

public class ScriptManager {

	/**Stores the scripts within the script manager, maps them all using a name for ease of access.*/
	protected ConcurrentHashMap<String, Script> scripts = new ConcurrentHashMap<String, Script>();
	/**Stores a bank of global variables, accessible to all scripts.*/
	protected ConcurrentHashMap<String, Object> variables = new ConcurrentHashMap<String, Object>();
	
	/**Runs a script with a given name, takes in the core class instance and a set of parameters to pass.*/
	public Object run(String name, Core core, Object... parameters) {
		try {
			/**First checks if the script given exists.*/
			if(scripts.containsKey(name)) {
				return scripts.get(name).run(parameters);
			} else { /**Else compiles and adds the script to the map.*/
				scripts.put(name, new Script(core, new FileHandler("scripts/" + name + ".sf")));
				return scripts.get(name).run(parameters);
			}
		} catch(Exception e) {
			core.showErrorDialog("An error has occurred whilst executing the script \"" + name + "\".", e);
			return null;
		}
	}
	
	/**Gets a map of all loaded scripts.*/
	public ConcurrentHashMap<String, Script> getScripts() {
		return this.scripts;
	}
	
	/**Gets a map of all stored global variables.*/
	public ConcurrentHashMap<String, Object> getVariables() {
		return this.variables;
	}
	
	/**Checks if the global variable map contains a given variable.*/
	public boolean containsVariable(String name) {
		return variables.containsKey(name);
	}
	
	/**Gets a variable of a given type.
	 * @param <T>*/
	@SuppressWarnings("unchecked")
	public <T> T getVariable(String name, Class<T> type) {
		return (T) variables.get(name);
	}
	
	/**Sets a variable in the global variable map.*/
	public void setVariable(String name, Object value) {
		variables.put(name, value);
	}
}