package hts.aiproject.utils.scripts;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import org.codehaus.janino.JavaSourceClassLoader;

import hts.aiproject.Core;
import hts.aiproject.file.FileHandler;

public class UserClassLoader<T> extends ClassLoader {
	
	/**Stores an instance of the core class.*/
	private Core core;
	/**Stores the compiler for the classes.*/
	private JavaSourceClassLoader compiler;
	/**Stores a map of the compiled class types with the class names.*/
	private ConcurrentHashMap<String, Class<T>> classes = new ConcurrentHashMap<String, Class<T>>();
	/**Constant array that stores the parameter types for the classes' constructors.*/
	private final Class<?>[] parameterTypes;
	
	/**Constructor for the class loader, creates the compilers for each class.*/
	/**Takes in the folder from which to load the classes, along with the types for the constructor.*/
	public UserClassLoader(Core core, String folder, Class<?>... parameterTypes) {
		this.core = core;
		this.parameterTypes = parameterTypes;
		/**Gets the list of all of the sub-folders within the folder.*/
		ArrayList<File> folders = FileHandler.getAllFoldersInFolder(new File(folder));
		/**Instantiates the compiler.*/
		this.compiler = new JavaSourceClassLoader(ClassLoader.getSystemClassLoader());
		this.compiler.setSourcePath(folders.toArray(new File[folders.size()]));
	}
	
	/**Loads a given class using the JavaSourceClassLoader.*/
	@Override
	public Class<?> loadClass(String className) {
		/**Compiles the class if not already compiled.*/
		if(classes.containsKey(className))
			compile(className);
		return classes.get(className);
	}
	
	/**Compiles the class with the given name.*/
	@SuppressWarnings("unchecked")
	public void compile(String className) {
		/**Adds the compiled class to the map after compilation.*/
		try {
			classes.put(className, (Class<T>) compiler.loadClass(className));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			/**Outputs the error message to a GUI dialog box.*/
			core.showErrorDialog("An error has occurred whilst compiling class \"" + className + "\".\n"
						+ "No such class was found in the class file!", e);
		}
	}
	
	/**Function that compiles a given class into the map and returns it.*/
	/**If the class is already compiled, returns the result.*/
	public T load(String className, Object... parameters) {
		/**Checks if the class has already been compiled into the map.*/
		if(classes.containsKey(className)) {
			/**Returns a new instance of the class, giving the desired parameters for the constructor.*/
			try {
				return classes.get(className)
						.getConstructor(parameterTypes)
						.newInstance(parameters);
			} catch (Exception e) {
				e.printStackTrace();
				/**Outputs the error message to a GUI dialog box.*/
				core.showErrorDialog("An error has occurred whilst instantiating class \"" + className + "\".", e);
			}
		} else {
			/**Compiles the class.*/
			compile(className);
			/**Returns the result of this class again, which will return an instance from the above.*/
			/**This is because the class class map will now contain data for the key.*/
			return load(className, parameters);
		}
		
		/**Outputs an error message to a GUI dialog box indicating no class was found.*/
		core.showErrorDialog("An error has occurred whilst compiling class \"" + className + "\".\nNo such class exists!", new ClassNotFoundException("Class \"" + className + "\" not found!"));
		/**If there's any exception, returns a null value.*/
		return null;
	}
	
	/**Gets the value of a given field in a class.*/
	public static Object getValue(Core core, Object instance, String field) {
		/**Checks to see whether the instance passed is initialised.*/
		if(instance != null) {
			try {
				/**Returns the value of the field within the given instance of the class.*/
				return instance.getClass().getField(field).get(instance);
			} catch (IllegalArgumentException | /**Checks if the parameter is an invalid type.*/
					IllegalAccessException | /**Checks if the field accessed is not allowed due to scope.*/
					NoSuchFieldException | /**Checks if a field with the given name exists.*/
					SecurityException e) { /**Checks if the field is allowed to be edited.*/
			/**Outputs an error message to a GUI dialog box indicating the error.*/
			/**Uses the name of the object*/
			core.showErrorDialog("An error has occurred whilst accessing the value of field \"" + field
						+ "\" in class \"" + instance.getClass().getSimpleName() + "\".", e);
			}
		} else {
			/**Outputs an error message to a GUI dialog box informing that the instance is not initialised.*/
			core.showErrorDialog("An error has occurred whilst accessing the value of field \"" + field + "\".\nInstance not initialised!",
					new NullPointerException("Instance passed not instantiated!"));
		}
		
		/**Returns null, as no value is stored.*/
		return null;
	}
	
	/**Sets the value of a given field in a class to the object given in the parameters.*/
	public static void setValue(Core core, Object instance, String field, Object value) {
		/**Checks to see whether the instance passed is initialised.*/
		if(instance != null) {
			try {
				/**Sets the value of the field within the given instance of the class.*/
				instance.getClass().getField(field).set(instance, value);
			} catch (IllegalArgumentException | /**Checks if the parameter is an invalid type.*/
						IllegalAccessException | /**Checks if the field accessed is not allowed due to scope.*/
						NoSuchFieldException | /**Checks if a field with the given name exists.*/
						SecurityException e) { /**Checks if the field is allowed to be edited.*/
				/**Outputs an error message to a GUI dialog box indicating the error.*/
				/**Uses the name of the object*/
				core.showErrorDialog("An error has occurred whilst editing field \"" + field
							+ "\" in class \"" + instance.getClass().getSimpleName() + "\".", e);
			}
		} else {
			/**Outputs an error message to a GUI dialog box informing that the instance is not initialised.*/
			core.showErrorDialog("An error has occurred whilst editing field \"" + field + "\".\nInstance not initialised!",
					new NullPointerException("Instance passed not instantiated!"));
		}
	}
}