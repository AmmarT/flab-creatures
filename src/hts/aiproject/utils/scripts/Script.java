package hts.aiproject.utils.scripts;

import java.lang.reflect.InvocationTargetException;

import org.codehaus.commons.compiler.CompileException;
import org.codehaus.janino.ScriptEvaluator;

import hts.aiproject.Core;
import hts.aiproject.file.FileHandler;

public class Script {

	/**Stores an instance of the core class.*/
	protected Core core;
	/**Stores the un-compiled source code for the script.*/
	protected String source;
	/**Stores the compiler for the script code.*/
	protected ScriptEvaluator evaluator;

	/**Constructor for the script class, takes in the code for the script as a string.*/
	public Script(Core core, String source) {
		this.core = core;
		this.source = source;
		/**Catches any errors that may occur in the compilation of the script.*/
		try {
			/**Initialises the script evaluation object.*/
			evaluator = new ScriptEvaluator();
			evaluator.setParentClassLoader(ClassLoader.getSystemClassLoader());
			/**Adds the core class to the list of variables that the script file will contain by default.*/
			evaluator.setParameters(new String[] {"core", "parameters"}, new Class<?>[] {Core.class, Object[].class});
			/**Allows the script to return an object.*/
			evaluator.setReturnType(Object.class);
			/**Compiles the script code into the evaluator.*/
			evaluator.cook(source);
		} catch (CompileException e) {
			/**Outputs the error message to a GUI dialog box.*/
			core.showErrorDialog("An error has occurred whilst compiling a script.\nSource:\n" + source, e);
		}
	}

	/**Constructor for the script class, takes in the code for the script from a given file.*/
	public Script(Core core, FileHandler scriptFile) {
		/**Uses the above constructor, passing in the contents of the file as the string for the parameters.*/
		this(core, "/*" + scriptFile.getPath().replace('\\', '/') + "*/" + scriptFile.getContents());
	}

	/**A function to execute the script, returns the output of the script as an object.*/
	/**Takes the core class in as a parameter to pass to the script.*/
	/**This allows the script to access any classes within the program without direct access.*/
	public Object run(Object... parameters) {
		/**Catches any runtime errors that may occur in the execution of the script and outputs the details to the console.*/
		try {
			/**Passes in the core class as a parameter upon evaluation of the script.*/
			/**Returns the resulting object returned.*/
			return evaluator.evaluate(new Object[] {core, parameters});
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			/**Outputs the error message to a GUI dialog box.*/
			core.showErrorDialog("An error has occurred whilst executing a script.\nSource:\n" + source, e);
			/**If the execution fails, outputs a null value.*/
			return null;
		}
	}

	/**Gets the source code of the script and returns it.*/
	public String getSource() {
		return source;
	}

	/**Gets the script evaluator object and returns it.*/
	public ScriptEvaluator getEvaluator() {
		return evaluator;
	}

	/**Sets the string storing the source of the script.*/
	public void setSource(String source) {
		this.source = source;
	}
}