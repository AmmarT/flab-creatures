package hts.aiproject.utils.scripts;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ClassLoaderUtils extends ClassLoader {
	
	private static Method m;
	
	static {
		try {
			m = ClassLoader.class.getDeclaredMethod("findLoadedClass", new Class[] {String.class});
	        m.setAccessible(true);
		} catch (NoSuchMethodException 
				| SecurityException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isClassLoaded(ClassLoader loader, String className) {
		try {
			return m.invoke(loader, className) != null;
		} catch (SecurityException 
				| IllegalAccessException 
				| IllegalArgumentException 
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return false;
	}
}