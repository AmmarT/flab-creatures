package hts.aiproject.physics;

import java.util.Vector;
import java.util.function.Predicate;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class PhysicsUtils {
	
	/**Gets all of the PhysicsObjects in a given World.*/
	public static Vector<PhysicsObject> getPhysicsObjects(World world) {
		/**Stores all of the PhysicsObjects in the World.*/
		Vector<PhysicsObject> objects = new Vector<PhysicsObject>();
		/**Gets all of the Body objects in the world.*/
		Array<Body> bodies = new Array<Body>();
		world.getBodies(bodies);
		
		/**Iterates over every Body in the world.*/
		for(Body body : bodies) {
			/**Checks if the Body has an associated PhysicsObject in its Fixtures.*/
			for(Fixture fixture : body.getFixtureList()) {
				if(fixture.getUserData() instanceof PhysicsObject) {
					/**Adds the PhysicsObject for the Fixture to the Vector storing them.*/
					objects.add((PhysicsObject) fixture.getUserData());
					/**Breaks out of the Fixture loop.*/
					break;
				}
			}
		}
		
		/**Returns the Vector of PhysicsObjects found.*/
		return objects;
	}
	
	/**Gets all of the PhysicsObjects in a given World that fit the given Predicate.*/
	public static Vector<PhysicsObject> getPhysicsObjects(World world, Predicate<PhysicsObject> removeIf) {
		/**Stores all of the PhysicsObjects in the World.*/
		Vector<PhysicsObject> objects = new Vector<PhysicsObject>();
		/**Gets all of the Body objects in the world.*/
		Array<Body> bodies = new Array<Body>();
		world.getBodies(bodies);
		
		/**Iterates over every Body in the world.*/
		for(Body body : bodies) {
			/**Checks if the Body has an associated PhysicsObject in its Fixtures.*/
			for(Fixture fixture : body.getFixtureList()) {
				if(fixture.getUserData() instanceof PhysicsObject) {
					/**Adds the PhysicsObject for the Fixture to the Vector storing them.*/
					objects.add((PhysicsObject) fixture.getUserData());
				}
			}
		}
		
		/**Removes any PhysicsObjects that fail the given Predicate.*/
		objects.removeIf(removeIf);
		/**Returns the Vector of PhysicsObjects found.*/
		return objects;
	}
}