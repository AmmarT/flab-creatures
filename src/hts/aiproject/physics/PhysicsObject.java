package hts.aiproject.physics;

import java.util.Vector;
import java.util.function.Predicate;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import hts.aiproject.Core;
import hts.aiproject.utils.Loadable;
import hts.aiproject.utils.maths.HTSMaths;

public class PhysicsObject implements Loadable {
	
	/**Stores the category bits for collision objects.*/
	public static final short COLLISION_CATEGORY = 0x1;
	/**Stores the category bits for players*/
	public static final short PLAYER_CATEGORY = 0x2;
	/**Stores the category bits for NPCs, friendly and hostile.*/
	public static final short NPC_CATEGORY = 0x4;
	/**Stores the category bits for projectiles.*/
	public static final short PROJECTILE_CATEGORY = 0x8;
	/**Stores the category bits for physics-enabled non-character objects.*/
	public static final short COLLIDABLE_SCENERY_CATEGORY = 0x10;
	/**Stores the category bits for non-physics-enabled non-character objects.*/
	public static final short META_CATEGORY = 0x20;
	
	/**Stores the mask for collision objects.
	 * Set to be everything but collision objects.*/
	public static final short COLLISION_MASK = ~COLLISION_CATEGORY;
	/**Stores the mask for player objects.
	 * Set to be everything but other players.*/
	public static final short PLAYER_MASK = ~PLAYER_CATEGORY;
	/**Stores the mask for NPCs, friendly and hostile.
	 * Set to be everything.*/
	public static final short NPC_MASK = -1;
	/**Stores the mask for projectiles.
	 * Set to be everything but other projectiles.*/
	public static final short PROJECTILE_MASK = ~PROJECTILE_CATEGORY;
	/**Stores the mask for physics-enabled non-character objects.
	 * Set to be everything.*/
	public static final short COLLIDABLE_SCENERY_MASK = -1;
	/**Stores the mask for non-physics-enabled non-character objects.
	 * Set to be nothing.*/
	public static final short META_MASK = ~-1;

	/**Stores the world this object is in.*/
	protected transient World world;
	/**Stores the physics object type of this object.*/
	protected PhysicsObjectType type;
	/**Stores the shape for the object.*/
	protected transient PolygonShape shape;
	/**Stores the body definition for the object.*/
	protected transient BodyDef bodyDef;
	/**Stores the body for the object.*/
	protected transient Body body;
	/**Stores the fixture definition for the object.*/
	protected transient FixtureDef fixtureDef;
	/**Stores the fixture for the object.*/
	protected transient Fixture fixture;
	/**Stores the size of the object.*/
	protected Vector2 size;
	/**Stores the mass of the object.*/
	protected float mass;
	
	/**Stores the object's current position.*/
	protected Vector2 position;
	/**Stores the object's current angle in radians.
	 * 0 degrees implies facing towards the positive x-axis whilst centered at the origin.*/
	protected float angle;
	/**Stores the object's current linear velocity.*/
	protected  Vector2 linearVelocity;
	/**Stores the object's current angular velocity.*/
	protected float angularVelocity;
	
	/**Constructor for the physics object, takes in the parameters of the body and creates the body in the world.*/
	public PhysicsObject(World world, PhysicsObjectType type, Vector2 position, Vector2 size, float mass) {
		this.world = world;
		this.type = type;
		this.mass = mass;
		this.size = size;
		this.position = position.cpy();
		this.linearVelocity = new Vector2();
		this.loadDef();
		this.loadBody(world);
	}
	
	/**No-arg constructor for de-serialisation.*/
	public PhysicsObject() {
		
	}
	
	/**Loads the PhysicsObject after de-serialisation.*/
	@Override
	public void load(Core core, Object... parameters) {
		this.loadDef();
		
		if(parameters.length > 0) {
			this.loadBody((World) parameters[0]);
			this.setPosition(position);
			this.setLinearVelocity(linearVelocity);
			this.setAngle(angle);
			this.setAngularVelocity(angularVelocity);
		}
	}
	
	/**Loads the definitions for the PhysicsObject.*/
	public void loadDef() {
		/**Sets the body definition parameters and creates the body.*/
		bodyDef = new BodyDef();
		bodyDef.awake = true;
		
		/**Determines the type of the object based on the physics object type.*/
		switch(type) {
		/**Sets the type to static if it is a collision border object.*/
		case COLLISION:
			bodyDef.type = BodyType.StaticBody;
			break;
		/**Sets the type to kinematic if it is a non-collision object.*/
		case META:
			bodyDef.type = BodyType.KinematicBody;
			break;
		/**Sets the type to dynamic if not.*/
		default:
			bodyDef.type = BodyType.DynamicBody;
			break;
		}
		
		/**Creates the shape of the object as a box from the size.*/
		shape = new PolygonShape();
		shape.setAsBox(size.x, size.y);
		
		/**Sets the fixture definition parameters and creates the fixture.*/
		fixtureDef = new FixtureDef();
		fixtureDef.filter.categoryBits = type.getCategoryBits();
		fixtureDef.filter.maskBits = type.getMaskBits();
		fixtureDef.shape = shape;
		fixtureDef.density = mass / (size.x * size.y);
		/**Sets the restitution of the object to 0 if a collision object, 1 otherwise.*/
		fixtureDef.restitution = type == PhysicsObjectType.COLLISION ? 0 : 1;
	}
	
	/**Loads the PhysicsObject into the world given in the parameters.*/
	public void loadBody(World world) {
		this.setWorld(world);
		body = world.createBody(bodyDef);
		body.setTransform(position.cpy(), body.getAngle());
		fixture = body.createFixture(fixtureDef);
		/**Adds this object to the fixture's user-data to allow for access during collisions.*/
		fixture.setUserData(this);
	}
	
	/**Disposes of the assets.*/
	public void dispose() {
		shape.dispose();
	}
	
	/**Ticks the PhysicsObject by updating the stored details about position, angle, and both linear and angular velocity.*/
	public void tick() {
		this.position = body.getPosition().cpy();
		this.angle = body.getAngle();
		this.linearVelocity = body.getLinearVelocity().cpy();
		this.angularVelocity = body.getAngularVelocity();
	}
	
	/**Finds the nearest PhysicsObject that satisfies the given Predicate.*/
	public PhysicsObject nearest(Predicate<PhysicsObject> removeIf) {
		/**Gets all of the PhysicsObjects in the world besides this.*/
		Vector<PhysicsObject> objects = PhysicsUtils.getPhysicsObjects(world, removeIf.or((o) -> o == this));
		/**Stores the closest object.*/
		PhysicsObject nearest = null;
		/**Stores the distance to the closest object.*/
		float minDistance = Float.MAX_VALUE;
		
		/**Iterates over every found PhysicsObject. Checks if the object is closer than the current closest*/
		for(PhysicsObject obj : objects) {
			float objToPos = position.cpy().sub(obj.position.cpy()).len2();
			
			if(objToPos < minDistance) {
				minDistance = objToPos;
				nearest = obj;
			}
		}
		
		return nearest;
	}
	
	/**Finds the nearest PhysicsObject to this.*/
	public PhysicsObject nearest() {
		return nearest((o) -> false);
	}
	
	/**Sets the angle of the object to face a given position.*/
	public void face(Vector2 position) {
		setAngle(MathUtils.atan2(position.y - this.position.y, position.x - this.position.x) + MathUtils.PI2);
	}
	
	/**Sets the angle of the object to face a given position in the nearest cardinal direction.*/
	public void faceDirection(Vector2 position) {
		face(position);
		snapDirection();
	}
	
	/**Sets the angle of the object to path towards a given position, avoiding obstacles.
	 * Uses the "potential fields" method to do this.
	 * Source: {@link https://stackoverflow.com/questions/45916071/2d-top-down-smooth-pathfinding-libgdx}*/
	public void fieldFace(Vector2 destination) {
		/**Gets the vector from this object to the destination.*/
		Vector2 posToDest = destination.cpy().sub(position.cpy()).nor();
		/**Gets all of the PhysicsObjects in the world that collide with this object.*/
		Vector<PhysicsObject> objects = PhysicsUtils.getPhysicsObjects(world, (o) -> o == this || !o.type.collides(type));
		
		/**Iterates over every found PhysicsObject.
		 * Adds a repulsive "force" for each of the objects inversely proportional to their distances.*/
		for(PhysicsObject obj : objects) {
			Vector2 objToPos = position.cpy().sub(obj.position.cpy());
			posToDest.add(objToPos.cpy().nor().scl(0.354f / objToPos.len2()));
		}
		
		/**Sets the angle to the angle to move in to path towards the destination.*/
		setAngle(posToDest.angleRad() + MathUtils.PI2);
	}
	
	/**Sets the direction of the object to path towards a given position, avoiding obstacles.
	 * Uses the "potential fields" method to do this.
	 * Source: {@link https://stackoverflow.com/questions/45916071/2d-top-down-smooth-pathfinding-libgdx}*/
	public void fieldFaceDirection(Vector2 destination) {
		fieldFace(destination);
		snapDirection();
	}
	
	/**Returns if the object is facing the given direction.*/
	public boolean isFacing(Direction direction) {
		return direction == getDirection();
	}
	
	/**Returns if the object is facing the given position.*/
	public boolean isFacing(Vector2 position) {
		/**Stores the original direction.*/
		Direction original = getDirection();
		/**Faces the position given.*/
		faceDirection(position);
		/**Gets if the direction is the same after facing.*/
		boolean same = original == getDirection();
		/**Resets the direction.*/
		setDirection(original);
		/**Returns if the directions were the same.*/
		return same;
	}
	
	/**Snaps the object's angle to face in the nearest direction.*/
	public void snapDirection() {
		setDirection(Direction.fromAngle(angle));
	}
	
	/**Applies an angular impulse to the object.*/
	public void applyAngularImpulse(float amount) {
		body.applyAngularImpulse(amount, true);
	}
	
	/**Applies a torque to the object.*/
	public void applyTorque(float amount) {
		body.applyTorque(amount, true);
	}
	
	
	/**Applies an impulse to the object in the given angle.*/
	public void applyImpulse(float amount, float angle) {
		body.applyLinearImpulse(new Vector2(HTSMaths.cos(angle) * amount, HTSMaths.sin(angle) * amount),
				body.getPosition(), true);
	}
	
	/**Applies a force to the object in the given angle.*/
	public void applyForce(float amount, float angle) {
		body.applyForce(new Vector2(HTSMaths.cos(angle) * amount, HTSMaths.sin(angle) * amount),
				body.getPosition(), true);
	}
	
	/**Applies an impulse to the object in the direction of travel.*/
	public void applyImpulse(float amount) {
		body.applyLinearImpulse(new Vector2(HTSMaths.cos(body.getAngle()) * amount, HTSMaths.sin(body.getAngle()) * amount),
				body.getPosition(), true);
	}
	
	/**Applies a force to the object in the direction of travel.*/
	public void applyForce(float amount) {
		body.applyForce(new Vector2(HTSMaths.cos(body.getAngle()) * amount, HTSMaths.sin(body.getAngle()) * amount),
				body.getPosition(), true);
	}

	public World getWorld() {
		return world;
	}
	
	public PhysicsObjectType getType() {
		return type;
	}

	public PolygonShape getShape() {
		return shape;
	}

	public BodyDef getBodyDef() {
		return bodyDef;
	}

	public Body getBody() {
		return body;
	}

	public FixtureDef getFixtureDef() {
		return fixtureDef;
	}

	public Fixture getFixture() {
		return fixture;
	}

	public Vector2 getSize() {
		return size;
	}

	public float getMass() {
		return mass;
	}
	
	public Vector2 getPosition() {
		return position;
	}
	
	public float getAngle() {
		return angle;
	}
	
	public Vector2 getLinearVelocity() {
		return linearVelocity;
	}
	
	public float getAngularVelocity() {
		return angularVelocity;
	}
	
	/**Gets the direction closest to the angle of the object.*/
	public Direction getDirection() {
		return Direction.fromAngle(getAngle());
	}
	
	public void setWorld(World world) {
		/**Checks if the world's aren't the same before setting.*/
		if(this.world != world) {
			/**Stores the previous position of the body.*/
			Vector2 prevPosition = getPosition();
			/**Stores the previous angle of the body.*/
			float prevAngle = getAngle();
			/**Stores the previous linear velocity of the body.*/
			Vector2 prevLinearVelocity = getLinearVelocity();
			/**Stores the previous angular velocity of the body.*/
			float prevAngularVelocity = getAngularVelocity();
			
			/**Checks if the body currently has a world.*/
			if(this.world != null) {
				/**Gets all the bodies in the world.*/
				Array<Body> bodies = new Array<Body>();
				this.world.getBodies(bodies);
				
				/**Checks if the body is in a world.*/
				if(bodies.contains(body, false)) {
					/**Removes the body from its current world.*/
					body.getWorld().destroyBody(body);
				}
			}
			
			/**Sets this room's world to the new world.*/
			this.world = world;
			/**Adds the body to the new room and sets the position and angle accordingly.*/
			body = world.createBody(bodyDef);
			fixture = body.createFixture(fixtureDef);
			body.setTransform(prevPosition, prevAngle);
			body.setLinearVelocity(prevLinearVelocity);
			body.setAngularVelocity(prevAngularVelocity);
		}
	}
	
	public void setPosition(Vector2 position) {
		Vector2 temp = position.cpy();
		this.position = temp;
		body.setTransform(position.cpy(), body.getAngle());
	}
	
	public void setAngle(float angle) {
		this.angle = angle;
		body.setTransform(body.getPosition(), angle);
	}
	
	public void setLinearVelocity(Vector2 linearVelocity) {
		Vector2 temp = linearVelocity.cpy();
		this.linearVelocity = temp;
		body.setLinearVelocity(linearVelocity.cpy());
	}
	
	public void setAngularVelocity(float angularVelocity) {
		this.angularVelocity = angularVelocity;
		body.setAngularVelocity(angularVelocity);
	}
	
	/**Sets the direction of the object.*/
	public void setDirection(Direction direction) {
		setAngle(direction.getAngle());
	}
}