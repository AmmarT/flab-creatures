package hts.aiproject.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import hts.aiproject.physics.PhysicsObject;
import hts.aiproject.physics.PhysicsObjectType;

public class Flab extends PhysicsObject {
	
	// Instance Variables
	protected float energy;
	protected float speed;
	protected float attitude;
	protected float intelligence;
	protected float sightAngle;
	protected float lastUpdateTime;
	
	public Flab(World world, Vector2 position, Vector2 size, float mass, float energy, float speed, 
			float attitude, float intelligence, float sightAngle) {
		super(world, PhysicsObjectType.NPC, position, size, mass);
		this.energy = energy; 
        this.speed = speed; 
        this.attitude = attitude; 
        this.intelligence = intelligence; 
        this.sightAngle = sightAngle;
	}

	public float getArea() {
		return size.x * size.y;
	}
	
	public double getMetabolismCost() {
		return size.len() + intelligence;
	}
	
	public double getMovementCost() {
		return (speed * speed) + (getArea() * getArea() * getArea());
	}
	
	public double getAttackCost() {
		return (speed * speed) + (1 / (1 + Math.abs(attitude))) + (intelligence / 2);
	}
	
	public double getThinkingCost() {
		return intelligence;
	}

	public double updateInSeconds(float timestamp) {
		float timePast = timestamp - lastUpdateTime;
		lastUpdateTime = timestamp;

		if(timePast >= (1/(1+intelligence))){
			//use network
			//return decision  
		} else {
			return Null;
		}
	}
}