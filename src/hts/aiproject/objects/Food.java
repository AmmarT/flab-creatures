package hts.aiproject.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import hts.aiproject.physics.PhysicsObject;
import hts.aiproject.physics.PhysicsObjectType;

public class Food extends PhysicsObject {

	public Food(World world, Vector2 position, Vector2 size) {
		super(world, PhysicsObjectType.COLLIDABLE_SCENERY, position, size, 0.25f);
	}
}