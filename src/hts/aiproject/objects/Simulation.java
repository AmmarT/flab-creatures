package hts.aiproject.objects;

import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.graphics;

import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public class Simulation {

    private World world;
    private Vector<Food> foods;
    private Vector<Flab> flabs;
    private float timeInSeconds;

    public void update(){
        timeInSeconds += Gdx.graphics.getDeltaTime();

        for(Flab flab : flabs){
            switch (flab.update(timeInSeconds)) {
                case "sleep":
                    break;
                case "move":
                    move(flab);
                    break;
                case "turn":
                    turn(flab);
                case "attack":
                    attack(flab, flab.target);
                case "reproduce":
                    flabs.add(flab.reproduce());
                default:
                    //no decision
                    break;
            }
        }

    }


}
