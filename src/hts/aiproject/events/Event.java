package hts.aiproject.events;

public class Event {
	
	/**Stores the name of the event script to execute.*/
	protected String name;
	/**Stores the parameters to pass to the event.*/
	protected Object[] parameters;
	/**Stores whether an event is cancelled or not.*/
	protected boolean cancelled;

	/**Constructor for the event class, stores the name of the script to execute and the parameters to pass.*/
	public Event(String name, Object... parameters) {
		this.name = name;
		/**Sets the parameters, if no parameters are entered in the variable argument then the array is empty.*/
		this.parameters = parameters;
	}

	public String getName() {
		return name;
	}

	public Object[] getParameters() {
		return parameters;
	}
	
	public boolean isCancelled() {
		return this.cancelled;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setParameters(Object... parameters) {
		this.parameters = parameters;
	}
	
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}