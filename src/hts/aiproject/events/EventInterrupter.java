package hts.aiproject.events;

public abstract class EventInterrupter {
	
	/**Stores the event to interrupt.*/
	protected String eventName;
	
	/**Takes in the name of the event to interrupt.*/
	public EventInterrupter(String eventName) {
		this.eventName = eventName;
	}
	
	/**Interrupts an event given the parameter of the event in question.*/
	public abstract void interrupt(Event e);
	
	public String getEventName() {
		return eventName;
	}
	
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
}