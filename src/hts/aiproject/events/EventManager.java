package hts.aiproject.events;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

import hts.aiproject.Core;
import hts.aiproject.utils.Queue;

public class EventManager {
	
	/**Stores a queue of all of the events to be executed this frame.*/
	private Queue<Event> events = new Queue<>();
	/**Stores a queue of all of the events to be executed on the next frame.*/
	private Queue<Event> nextEvents = new Queue<Event>();
	/**Stores a HashMap of all of the queues of events for different delay periods in ticks.*/
	private ConcurrentHashMap<Integer, Queue<Event>> delayedEvents = new ConcurrentHashMap<Integer, Queue<Event>>();
	/**Stores a queue of all of the event interruptions to be executed this frame.*/
	private Queue<EventInterrupter> interruptions = new Queue<EventInterrupter>();
	/**Flag that stores whether the event queue is being iterated over or not.*/
	private boolean isEventRunning;
	
	/**Execution function for the event manager, this function runs all of the events in the queue.
	 * The iteration is controlled by the isEmpty() function and applies a not gate.
	 * The result of this is that the queue is continually reduced until it is empty.
	 * Takes in an instance of the core class to pass to the scripts.*/
	public void run(Core core) {
		/**Sets the event flag to true.*/
		isEventRunning = true;
		
		/**Iterates as long as elements remain in the queue.*/
		while(!events.isEmpty()) {
			/**Gets the event from the queue.*/
			Event event = (Event) events.dequeue();
			/**Creates a temporary queue of the interruptions to be handled for this event.*/
			Queue<EventInterrupter> tempInterrupts = new Queue<EventInterrupter>();
			
			/**Iterates over each interruption.*/
			while(!interruptions.isEmpty()) {
				/**Gets the interruption from the queue.*/
				EventInterrupter interrupter = interruptions.dequeue();
				/**Enqueues the event into the temporary queue.*/
				tempInterrupts.enqueue(interrupter);
				/**Runs the event interruption for the current event if appropriately named.*/
				if(event.getName().equals(interrupter.getEventName()))
					interrupter.interrupt(event);
			}
			
			/**Adds all used interruptions back to the queue from the temporary queue.*/
			while(!tempInterrupts.isEmpty())
				interruptions.enqueue(tempInterrupts.dequeue());
			
			/**First checks that the event is not cancelled.*/
			if(!event.isCancelled()) {
				/**Gets the script from the script manager with that name and executes the script dequeued.*/
				/**The queue stores the names of the scripts to execute, hence the queue contains the keys for the map.*/
				core.getScriptManager().run("events/" + event.getName(), core, event.getParameters());
			}
		}
		
		/**Disables the event flag by setting it to false.*/
		isEventRunning = false;
		/**Transfers the contents of the next frame's event queue to that of this frame.*/
		while(!nextEvents.isEmpty())
			events.enqueue(nextEvents.dequeue());
		/**Shifts the delayed events queue down a tick.*/
		shiftDownDelayed();
		/**Empties the interruption queue.*/
		interruptions.clear();
	}
	
	/**Shifts down the delayed events queue into the regular queue.*/
	public void shiftDownDelayed() {
		/**Checks if the delayed event queue contains any queued events for next frame.*/
		if(delayedEvents.containsKey(0)) {
			/**Gets the queue for the first tick.*/
			Queue<Event> queue = delayedEvents.get(0);
			
			/**Adds all the events to the regular events queue.*/
			while(!queue.isEmpty()) {
				events.enqueue(queue.dequeue());
			}
		}
		
		/**Removes the tick delays for 0 from the map.*/
		delayedEvents.remove(0);
		/**Gets the set of all loaded delay queues in order.*/
		ArrayList<Integer> tickDelays = new ArrayList<Integer>(delayedEvents.keySet());
		Collections.sort(tickDelays);
		
		/**Iterates over all loaded tick delays in order.*/
		for(int tickDelay : tickDelays) {
			/**Gets the event queue for the tick delay.*/
			Queue<Event> queue = delayedEvents.get(tickDelay);
			
			/**Adds all events from this queue to the previous tick delay's queue.*/
			while(!queue.isEmpty()) {
				Event event = queue.dequeue();
				addDelayed(event.name, tickDelay - 1, event.parameters);
			}
		}
	}
	
	/**Adds the given event to the correct queue.
	 * If the event queue is being iterated through, adds to the next frame's event queue.
	 * Checks this using a flag that is only active during the iteration.*/
	public void add(String name, Object... parameters) {
		/**Creates the event encapsulating this data.*/
		Event event = new Event(name, parameters);
		
		/**Checks if an event is running.*/
		if(isEventRunning) {
			/**Adds the event to next frame's event queue.*/
			nextEvents.enqueue(event);
		} else {
			/**Adds the event to this frame's event queue.*/
			events.enqueue(event);
		}
	}
	
	/**Adds the given event to the next frame's event queue.*/
	public void addDelayed(String name, int tickDelay, Object... parameters) {
		/**Creates the event encapsulating this data.*/
		Event event = new Event(name, parameters);
		
		/**Checks if a queue exists for the current delay period specified.*/
		if(delayedEvents.containsKey(tickDelay)) {
			/**If so, adds it to the queue for the tick delay.*/
			delayedEvents.get(tickDelay).enqueue(event);
		} else {
			/**If not, creates a queue for the tick delay period.
			 * Then adds the event to the newly created queue.*/
			delayedEvents.put(tickDelay, new Queue<Event>());
			delayedEvents.get(tickDelay).enqueue(event);
		}
	}
	
	/**Interrupts an event by adding the event interrupter from the parameters to the queue.
	 * If the event queue is being iterated through, adds to the next frame's event queue.*/
	public void interrupt(EventInterrupter interruption) {
		/**Adds the interruption to this frame's interruption queue.*/
		interruptions.enqueue(interruption);
	}
	
	public Queue<Event> getEvents() {
		return this.events;
	}
	
	public Queue<Event> getNextEvents() {
		return this.nextEvents;
	}
	
	public Queue<EventInterrupter> getInterruptions() {
		return this.interruptions;
	}
	
	public boolean isEventRunning() {
		return this.isEventRunning;
	}
}