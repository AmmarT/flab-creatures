package hts.aiproject.graphics;

import java.util.HashMap;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import hts.aiproject.Core;
import hts.aiproject.file.FileHandler;
import hts.aiproject.utils.Loadable;

public class SpriteSheet implements Loadable {

	/**Stores the location of the sprite-sheet texture.*/
	protected String textureLocation;
	/**Stores the sprite-sheet texture.*/
	protected transient Texture texture;
	/**Stores a 2D array of all contained sprites.*/
	protected transient TextureRegion[][] sprites;
	/**Stores the width and height of each sprite within the sheet in pixels.*/
	protected int spriteWidth, spriteHeight;
	/**Stores the animations mapped to their names created from the sheet.*/
	protected HashMap<String, Animation> animations = new HashMap<String, Animation>();
	
	/**Constructor for the sprite-sheet, takes in the texture, width and height of the sprites.*/
	public SpriteSheet(Texture texture, int spriteWidth, int spriteHeight) {
		this.texture = texture;
		this.spriteWidth = spriteWidth;
		this.spriteHeight = spriteHeight;
		/**Splits the texture into the 2D array.*/
		sprites = new TextureRegion(texture).split(spriteWidth, spriteHeight);
	}
	
	/**Constructor for the sprite-sheet, takes in the texture location, width and height of the sprites.*/
	public SpriteSheet(Core core, String textureLocation, int spriteWidth, int spriteHeight) {
		this.textureLocation = textureLocation;
		this.spriteWidth = spriteWidth;
		this.spriteHeight = spriteHeight;
		this.load(core);
		/**Splits the texture into the 2D array.*/
		sprites = new TextureRegion(texture).split(spriteWidth, spriteHeight);
	}
	
	/**Constructor for the sprite-sheet, reads in the parameters from the given file.*/
	public SpriteSheet(Core core, FileHandler spriteSheetFile) {
		/**Passes the contents of the file to the below constructor.*/
		this(core, spriteSheetFile.getContents());
	}
	
	/**Constructor for the sprite-sheet, reads in the parameters from the given file.*/
	public SpriteSheet(Core core, String spriteSheet) {
		/**First reads in the contents of the file as a string and splits it by lines.*/
		String[] lines = spriteSheet.trim().split("\n");
		/**Gets the texture location from the first line.*/
		this.textureLocation = lines[0].trim();
		/**Loads the texture from the texture location.*/
		this.load(core);
			
		/**Takes the width and height to be stored in the second and third lines respectively.*/
		/**Parses both back into integers.*/
		/**Catches an error where the lines may contain other characters.*/
		try {
			this.spriteWidth = Integer.parseInt(lines[1].trim());
			this.spriteHeight = Integer.parseInt(lines[2].trim());
			/**Gets the list of animations from the animations file.*/
			String[] animations = new FileHandler(lines[3].trim()).getContents().trim().split("\n");
				
			/**Iterates over each line of the animations file and creates animations.*/
			for(int i = 0; i < animations.length; i++) {
				/**Splits the line into its constituent components.*/
				String[] parts = animations[i].split(", ");
				/**The first part is taken as the animation name.*/
				/**The second part is taken as the animation's row in the sheet.*/
				/**The third part is taken as the starting index and the fourth as the ending index.*/
				String animationName = parts[0].trim();
				float duration = Float.parseFloat(parts[1].trim());
				int row = Integer.parseInt(parts[2].trim()),
							start = Integer.parseInt(parts[3].trim()),
							end = Integer.parseInt(parts[4].trim());
				boolean reversed = Boolean.parseBoolean(parts[5].trim());
				/**Adds the animation to the map.*/
				this.animations.put(animationName, new Animation(duration, row, start, end, reversed));
			}
		} catch(NumberFormatException e) {
			core.showErrorDialog("An error has occurred whilst loading a spritesheet!\nSpritesheet:\n" + spriteSheet, e);
		}
		
		/**Creates the 2D array to store the sprites.*/
		this.sprites = new TextureRegion(texture).split(spriteWidth, spriteHeight);
	}
	
	/**Constructor for the sprite-sheet, reads in the parameters from the given file
	 * Also takes in a HashMap of String-String mappings of replacements for the animation file.*/
	public SpriteSheet(Core core, String spriteSheet, HashMap<String, String> replacements) {
		/**First reads in the contents of the file as a string and splits it by lines.*/
		String[] lines = spriteSheet.trim().split("\n");
		/**Gets the texture location from the first line.*/
		this.textureLocation = lines[0].trim();
		/**Loads the texture from the texture location.*/
		this.load(core);
			
		/**Takes the width and height to be stored in the second and third lines respectively.*/
		/**Parses both back into integers.*/
		/**Catches an error where the lines may contain other characters.*/
		try {
			this.spriteWidth = Integer.parseInt(lines[1].trim());
			this.spriteHeight = Integer.parseInt(lines[2].trim());
			/**Gets the entire list of animations from the animations file.*/
			String animationSheet = new FileHandler(lines[3].trim()).getContents().trim();
			/**Iterates over all values in the replacement map and replaces them.*/
			for(String key : replacements.keySet())
				animationSheet = animationSheet.replaceAll(key, replacements.get(key));
			/**Splits the animation sheet into a list of animation strings.*/
			String[] animations = animationSheet.trim().split("\n");
			
			/**Iterates over each line of the animations file and creates animations.*/
			for(int i = 0; i < animations.length; i++) {
				/**Splits the line into its constituent components.*/
				String[] parts = animations[i].split(", ");
				/**The first part is taken as the animation name.*/
				/**The second part is taken as the animation's row in the sheet.*/
				/**The third part is taken as the starting index and the fourth as the ending index.*/
				String animationName = parts[0].trim();
				float duration = Float.parseFloat(parts[1].trim());
				int row = Integer.parseInt(parts[2].trim()),
							start = Integer.parseInt(parts[3].trim()),
							end = Integer.parseInt(parts[4].trim());
				boolean reversed = Boolean.parseBoolean(parts[5].trim());
				/**Adds the animation to the map.*/
				this.animations.put(animationName, new Animation(duration, row, start, end, reversed));
			}
		} catch(NumberFormatException e) {
			core.showErrorDialog("An error has occurred whilst loading a spritesheet!\nSpritesheet:\n" + spriteSheet, e);
		}
		
		/**Creates the 2D array to store the sprites.*/
		this.sprites = new TextureRegion(texture).split(spriteWidth, spriteHeight);
	}
	
	/**Gets the index of the frame an animation is currently on using the current time.*/
	public int getFrameIndex(String animationName, float stateTime) {
		/**Gets the animation.*/
		Animation animation = animations.get(animationName);
		/**Modularly divides the time by the animation duration to clip it cyclically.*/
		/**Then calculates the percentage through the animation that the time is currently at.*/
		float relativeTime = stateTime % animation.duration,
				elapsedPercentile = relativeTime / animation.duration;
		/**Calculates the current frame through the animation.*/
		/**Rounds the elapsed percentile's product with the animation's width in frames.*/
		/**Gets the width in frames by subtracting the end frame index from the start.*/
		int elapsedFrames = Math.round(elapsedPercentile * (animation.end - animation.start));
		/**Returns the elapsed frames plus the start frame.*/
		/**This gets the frame index that the animation is currently on.*/
		return animation.start + elapsedFrames;
	}
	
	/**Gets the frame an animation is currently on using the current time.*/
	public TextureRegion getFrame(String animationName, float stateTime) {
		/**Returns the index in the row equal to the elapsed frames plus the start frame.*/
		/**This gets the frame that the animation is currently on.*/
		return sprites[animations.get(animationName).row][getFrameIndex(animationName, stateTime)];
	}
	
	/**Loads the texture from the given texture location.*/
	@Override
	public void load(Core core, Object... parameters) {
		this.texture = core.getTextureManager().getTexture(textureLocation);
	}

	public Texture getTexture() {
		return texture;
	}

	public TextureRegion[][] getSprites() {
		return sprites;
	}

	public int getSpriteWidth() {
		return spriteWidth;
	}

	public int getSpriteHeight() {
		return spriteHeight;
	}

	public HashMap<String, Animation> getAnimations() {
		return animations;
	}
}