package hts.aiproject.graphics;

public class Animation {
	
	/**Stores the duration of the animation in seconds.*/
	protected float duration;
	/**Stores the row, starting index and ending index of the animation.*/
	protected int row, start, end;
	/**Stores whether the animation should be played in reverse.*/
	protected boolean reversed;
	
	/**Constructor for the animation class, takes in the indexes and whether to reverse the animation.*/
	public Animation(float duration, int row, int start, int end, boolean reversed) {
		this.duration = duration;
		this.row = row;
		this.start = start;
		this.end = end;
		this.reversed = reversed;
	}

	/**Getters*/
	public float getDuration() {
		return duration;
	}

	public int getRow() {
		return row;
	}

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	public boolean isReversed() {
		return reversed;
	}
}