package hts.aiproject.graphics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class GraphicsUtils {
	
	/**Generates a random colour.*/
	public static Color randomColour() {
		return new Color((float) Math.random(), (float) Math.random(), (float) Math.random(), 1);
	}
	
	/**Flattens a 2D sprite-sheet into a 1D list of textures.*/
	public static TextureRegion[] flattenSpriteSheet(TextureRegion[][] sheet) {
		if(sheet.length > 0) {
			TextureRegion[] list = new TextureRegion[sheet.length * sheet[0].length];
			int step = 0;
			
			for(int i = 0; i < sheet.length; i++) {
				for(int j = 0; j < sheet[i].length; j++) {
					list[step++] = sheet[i][j];
				}
			}
			
			return list;
		} else return new TextureRegion[0];
	}
	
	/**Trims the number of textures in a list*/
	public static TextureRegion[] trimFrames(int frameCount, TextureRegion... list) {
		if(frameCount < list.length) {
			TextureRegion[] trimmed = new TextureRegion[frameCount];
			for(int i = 0; i < frameCount; i++)
				trimmed[i] = list[i];
			return trimmed;
		} else return list;
	}
	
	/**Draws the given texture on the given batch.*/
	public static void draw(SpriteBatch batch, TextureRegion texture, Vector2 position) {
		batch.draw(texture, position.x, position.y);
	}
	
	/**Draws the given texture on the given batch with the given size.*/
	public static void draw(SpriteBatch batch, TextureRegion texture, Vector2 position, Vector2 size) {
		batch.draw(texture, position.x, position.y, size.x, size.y);
	}
	
	/**Draws the given texture on the given batch with the given colour.*/
	public static void draw(SpriteBatch batch, TextureRegion texture, Vector2 position, Color colour) {
		/**Stores the original colour of the batch.*/
		Color original = batch.getColor().cpy();
		/**Sets the new colour of the batch.*/
		batch.setColor(colour.cpy().mul(original.cpy()));
		batch.draw(texture, position.x, position.y);
		/**Sets the colour of the batch back to the original.*/
		batch.setColor(original.cpy());
	}
	
	/**Draws the given texture on the given batch at the given size with the given colour.*/
	public static void draw(SpriteBatch batch, TextureRegion texture, Vector2 position, Vector2 size, Color colour) {
		/**Stores the original colour of the batch.*/
		Color original = batch.getColor().cpy();
		/**Sets the new colour of the batch.*/
		batch.setColor(colour.cpy().mul(original.cpy()));
		batch.draw(texture, position.x, position.y, size.x, size.y);
		/**Sets the colour of the batch back to the original.*/
		batch.setColor(original.cpy());
	}
	
	/**Draws the given texture on the given batch at the given position and angle.*/
	public static void draw(SpriteBatch batch, TextureRegion texture, Vector2 position, float rads) {
		batch.draw(texture,
				position.x, position.y,
				texture.getRegionWidth() / 2, texture.getRegionHeight() / 2,
				texture.getRegionWidth(), texture.getRegionHeight(),
				1, 1, rads * (180 / MathUtils.PI), false);
	}
	
	/**Draws the given texture on the given batch at the given position and angle.*/
	public static void draw(SpriteBatch batch, TextureRegion texture, Vector2 position, float rads, Color colour) {
		/**Stores the original colour of the batch.*/
		Color original = batch.getColor().cpy();
		/**Sets the new colour of the batch.*/
		batch.setColor(colour.cpy().mul(original.cpy()));
		/**Draws the texture.*/
		draw(batch, texture, position, rads);
		/**Sets the colour of the batch back to the original.*/
		batch.setColor(original.cpy());
	}
	
	/**Draws the given texture on the given batch at the given position, size, and angle.*/
	public static void draw(SpriteBatch batch, TextureRegion texture, Vector2 position, Vector2 size, float rads) {
		Vector2 rotScale = size.cpy().scl(1 / size.y);
		rotScale.set(rotScale.y, rotScale.x);
		rotScale.x /= rotScale.y * 0.5f;
		
		batch.draw(texture,
				position.x - size.x, position.y - size.y,
				size.x, size.y,
				size.x * 2, size.y * 2,
				rotScale.x, rotScale.y, rads * (180 / MathUtils.PI), false);
	}
	
	/**Draws the given texture on the given batch at the given position and angle.
	 * Also takes in the colour to draw in.*/
	public static void draw(SpriteBatch batch, TextureRegion texture, Vector2 position, Vector2 size, float rads, Color colour) {
		/**Stores the original colour of the batch.*/
		Color original = batch.getColor().cpy();
		/**Sets the new colour of the batch.*/
		batch.setColor(colour.cpy().mul(original.cpy()));
		/**Draws the texture.*/
		draw(batch, texture, position, size, rads);
		/**Sets the colour of the batch back to the original.*/
		batch.setColor(original.cpy());
	}
	
	/**Draws the given text using the given batch and font at the given position.*/
	public static void draw(SpriteBatch batch, BitmapFont font, String text, Vector2 position) {
		font.draw(batch, text, position.x, position.y);
	}
	
	/**Draws the given text using the given batch and font at the given position and colour.*/
	public static void draw(SpriteBatch batch, BitmapFont font, String text, Vector2 position, Color colour) {
		/**Stores the original colour of the font.*/
		Color original = font.getColor().cpy();
		/**Sets the new colour of the font.*/
		font.setColor(colour.cpy().mul(original.cpy()));
		font.draw(batch, text, position.x, position.y);
		/**Sets the colour of the font back to the original.*/
		font.setColor(original.cpy());
	}
	
	/**Draws the given text using the given batch and font at the given position and angle.*/
	public static void draw(SpriteBatch batch, BitmapFont font, String text, Vector2 position, float rads) {
		/**Stores the original and new transformation matrices for the batch.*/
		Matrix4 original = batch.getTransformMatrix().cpy(), transformed = original.cpy();
		/**Rotates and translates the new transformation matrix appropriately.*/
		transformed.rotate(new Vector3(0, 0, 1), -rads * 180 / MathUtils.PI)
					.trn(new Vector3(position, 0));
		batch.setTransformMatrix(transformed.cpy());
		/**Draws the font at (0,0) as the position has been set by the transformation.*/
		font.draw(batch, text, 0, 0);
		/**Sets the transformation matrix for the batch back to the original.*/
		batch.setTransformMatrix(original.cpy());
	}
	
	/**Draws the given text using the given batch and font at the given position, angle, and colour.*/
	public static void draw(SpriteBatch batch, BitmapFont font, String text, Vector2 position, float rads, Color colour) {
		/**Stores the original and new transformation matrices for the batch.*/
		Matrix4 original = batch.getTransformMatrix().cpy(), transformed = original.cpy();
		/**Rotates and translates the new transformation matrix appropriately.*/
		transformed.rotate(new Vector3(0, 0, 1), -rads * 180 / MathUtils.PI)
					.trn(new Vector3(position, 0));
		batch.setTransformMatrix(transformed.cpy());
		/**Draws the font at (0,0) as the position has been set by the transformation.
		 * Draws it coloured using the coloured rendering method.*/
		draw(batch, font, text, new Vector2(0, 0), colour);
		/**Sets the transformation matrix for the batch back to the original.*/
		batch.setTransformMatrix(original.cpy());
	}
}