package hts.aiproject.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import hts.aiproject.Core;
import hts.aiproject.file.FileHandler;

public class AnimatedTexture {
	
	/**Stores the current time for the animation.*/
	protected float time;
	/**Stores the duration of the animation in seconds.*/
	protected float duration;
	/**Stores the textures in the animation.*/
	protected transient TextureRegion[] textures;
	
	/**Constructor for the AnimatedTexture, takes in a file containing the details about the textures.*/
	public AnimatedTexture(Core core, FileHandler file) {
		this(core, file.getContents().trim());
	}
	
	/**Constructor for the AnimatedTexture, takes in a string containing the details about the textures.*/
	public AnimatedTexture(Core core, String spriteSheet) {
		/**First reads in the contents of the file as a string and splits it by lines.*/
		String[] lines = spriteSheet.trim().split("\n");
		/**Gets the texture location from the first line.*/
		String textureLocation = lines[0].trim();
		/**Gets the duration from the second line.*/
		this.duration = Float.parseFloat(lines[1].trim());
		/**Loads the TextureRegion[] using the data from line 3 onwards.*/
		this.textures = GraphicsUtils.trimFrames(Integer.parseInt(lines[2].trim()),
				GraphicsUtils.flattenSpriteSheet(new TextureRegion(core.getTextureManager().getTexture(textureLocation))
						.split(Integer.parseInt(lines[3].trim()), Integer.parseInt(lines[4].trim()))));
	}

	/**Constructor for the AnimatedTexture, takes in the sprite-sheet to create the textures from.*/
	public AnimatedTexture(float duration, TextureRegion spriteSheet, int frameWidth, int frameHeight) {
		this.textures = GraphicsUtils.flattenSpriteSheet(spriteSheet.split(frameWidth, frameHeight));
		this.duration = duration;
	}
	
	/**Constructor for the AnimatedTexture, takes in the sprite-sheet to create the textures from.
	 * Also takes in the number of textures from the sheet to use.*/
	public AnimatedTexture(float duration, TextureRegion spriteSheet, int frameWidth, int frameHeight, int noOfTextures) {
		this(duration, spriteSheet, frameWidth, frameHeight);
		this.textures = GraphicsUtils.trimFrames(noOfTextures, this.textures);
	}
	
	/**Constructor for the AnimatedTexture, takes in the textures to use for the texture.*/
	public AnimatedTexture(float duration, TextureRegion... textures) {
		this.textures = textures;
		this.duration = duration;
	}
	
	/**Gets the texture at the current time.*/
	public TextureRegion getCurrentFrame() {
		/**Modularly divides the time by the animation duration to clip it cyclically.*/
		/**Then calculates the percentage through the animation that the time is currently at.*/
		/**Calculates the current frame through the animation.*/
		/**Rounds the elapsed percentile's product with the animation's width in frames.*/
		/**Gets the width in frames by subtracting the end frame index from the start.*/
		return textures[Math.round(((time % duration) / duration) * (textures.length - 1))];
	}
	
	/**Updates time for the animation.*/
	public void update() {
		time = time >= duration ? 0 : time + Gdx.graphics.getDeltaTime();
	}
	
	/**Draws the texture at the given position.*/
	public void draw(SpriteBatch batch, Vector2 position) {
		GraphicsUtils.draw(batch, getCurrentFrame(), position);
	}
	
	/**Draws the texture at the given position and size.*/
	public void draw(SpriteBatch batch, Vector2 position, Vector2 size) {
		GraphicsUtils.draw(batch, getCurrentFrame(), position, size);
	}
	
	/**Draws the texture at the given position with the given colour.*/
	public void draw(SpriteBatch batch, Vector2 position, Color colour) {
		GraphicsUtils.draw(batch, getCurrentFrame(), position, colour);
	}
	
	/**Draws the texture at the given position and size with the given colour.*/
	public void draw(SpriteBatch batch, Vector2 position, Vector2 size, Color colour) {
		GraphicsUtils.draw(batch, getCurrentFrame(), position, size, colour);
	}
	
	/**Draws the texture at the given position, size, and angle.*/
	public void draw(SpriteBatch batch, Vector2 position, Vector2 size, float rads) {
		GraphicsUtils.draw(batch, getCurrentFrame(), position, size, rads);
	}
	
	/**Draws the texture at the given position, size, and angle.
	 * Also takes in the colour to draw in.*/
	public void draw(SpriteBatch batch, Vector2 position, Vector2 size, float rads, Color colour) {
		GraphicsUtils.draw(batch, getCurrentFrame(), position, size, rads, colour);	
	}
}