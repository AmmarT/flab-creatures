package hts.aiproject.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class TextRenderer {
	
	/**Stores the max font size and the width of the border around the rendered text.*/
	public static final int MAX_SIZE = 128, BORDER_WIDTH = 1;
	/**Stores an array of all the fonts generated.*/
    private transient BitmapFont[] text = new BitmapFont[MAX_SIZE];
    /**Stores the generator object used to generate the fonts for the array.*/
    private transient FreeTypeFontGenerator generator;
    /**Stores the parameters for the font generator.*/
    private transient FreeTypeFontParameter params;
    
    /**Takes in the location of the font file as a parameter.
     * Also takes in an value determining whether the fonts should simply scale from an existing font.
     * The value refers to the font-size to scale from.*/
    public void load(String fontLocation) {
    	FreeTypeFontGenerator.setMaxTextureSize(FreeTypeFontGenerator.NO_MAXIMUM);
    	/**Initialises the generator using the file location given.*/
    	generator = new FreeTypeFontGenerator(Gdx.files.local(fontLocation));
    	params = new FreeTypeFontParameter();
    	
    	/**Sets the parameters of the object constant for each font, regardless of size.*/
    	params.borderWidth = BORDER_WIDTH;
        params.borderColor = Color.BLACK;
        params.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        params.magFilter = TextureFilter.Nearest;
        params.minFilter = TextureFilter.Nearest;
        params.genMipMaps = true;
        params.kerning = true;
        params.genMipMaps = true;
    }
    
    /**Disposes of any loaded fonts.*/
    public void dispose() {
    	/**Iterates over every font.*/
    	for(BitmapFont font : text) {
    		/**Checks if the font is loaded before disposing of it.*/
    		if(font != null) {
    			font.dispose();
    		}
    	}
    }
    
    /**Loads a font of a certain size given the generator and parameters.*/
    public void generateFont(int size) {
    	/**Sets the size of the font, scaling for the pixels-per-inch for the device the game is being executed on.*/
        params.size = Math.round(((size + 1) * Gdx.graphics.getDensity()));
        /**Generates the font given the parameter.*/
        text[size] = generator.generateFont(params);
        /**Sets the flag allowing colour mark-up tags to be used in text rendered.*/
        text[size].getData().markupEnabled = true;
    }
    
    /**Generates all of the fonts up to a certain size by scaling.*/
    public void generateScaledFonts(int size) {
    	/**Generates the font at size 72 to start with.*/
    	generateFont(72);
    }
    
    /**Gets a font of a given size, returns the smallest size if the parameter is too small.*/
    /**Alternatively, if the size given is too large, returns the largest size.*/
    public BitmapFont getSize(int size) {
    	/**Uses a ternary conditional operator to condense the conditional statements required on to one line.*/
    	return text[size > 0 ? (size <= text.length ? size - 1 : text.length - 1) : 1];
    }
    
    /**Gets the width in pixels of a given string with a given font.*/
    public static float getTextWidth(BitmapFont font, String text) {
    	return new GlyphLayout(font, text).width;
    }
    
    /**Gets the height in pixels of a given string with a given font.*/
    public static float getTextHeight(BitmapFont font, String text) {
    	return new GlyphLayout(font, text).height;
    }
}