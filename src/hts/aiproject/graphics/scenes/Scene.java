package hts.aiproject.graphics.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import hts.aiproject.Core;

public abstract class Scene extends Stage {
	
	/**Stores an instance of the core class.*/
	protected transient Core core;
	/**Stores the name of the scene.*/
	protected transient String name;
	
	/**Constructor for the scene, takes in the core class, as well as the width and height of the scene.*/
	public Scene(Core core, String name, float width, float height) {
		/**Extends the scene to fill the width and height specified.*/
		super(new ExtendViewport(width, height, new OrthographicCamera(width, height)), new SpriteBatch());
		this.core = core;
		this.name = name;
	}
	
	/**Alternative constructor for the scene, uses the window width and height as the scene width and height.*/
	public Scene(Core core, String name) {
		this(core, name, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}
	
	/**Abstract method to handle logic for all objects.*/
	public abstract void tick();
	
	/**Gets the core class instance.*/
	public Core getCore() {
		return this.core;
	}
	
	/**Gets the name of the scene.*/
	public String getName() {
		return name;
	}
}