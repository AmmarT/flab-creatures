package hts.aiproject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Stack;
import java.util.logging.LogManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.GdxNativesLoader;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import hts.aiproject.events.EventManager;
import hts.aiproject.graphics.TextRenderer;
import hts.aiproject.graphics.TextureManager;
import hts.aiproject.graphics.scenes.Scene;
import hts.aiproject.sound.SoundManager;
import hts.aiproject.utils.input.InputManager;
import hts.aiproject.utils.scripts.ScriptManager;
import hts.aiproject.utils.scripts.UserClassLoader;

public class Core extends Game {
	
	/**Entry method for single-player application.*/
	public static void main(String[] args) {
		/**Stores the attributes of the created window within its fields.*/
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		/**Sets the desired attributes of the created window.*/
		cfg.width = 1440 /*(int) Toolkit.getDefaultToolkit().getScreenSize().width*/;
		cfg.height = 810 /*(int) Toolkit.getDefaultToolkit().getScreenSize().height*/;
		cfg.title = "Learn2Learn";
		cfg.fullscreen = false;
		cfg.resizable = false;
		cfg.vSyncEnabled = true;
		/**Creates a new window using these defined parameters as well as a new instance of this class.*/
		new LwjglApplication(new Core(), cfg);
	}

	/**Stores the input manager for the game.*/
	protected InputManager inputManager;
	/**Stores the script manager for the game.*/
	protected ScriptManager scriptManager;
	/**Stores the event manager for the game.*/
	protected EventManager eventManager;
	/**Stores the texture manager for the game.*/
	protected TextureManager textureManager;
	/**Stores the sound manager for the game.*/
	protected SoundManager soundManager;
	/**Stores the text renderer for the game.*/
	protected TextRenderer textRenderer;
	
	/**Stores the Scene loader for the game.*/
	protected UserClassLoader<Scene> sceneLoader;
	/**Stores the foreground stage to draw dialog boxes to.*/
	protected Stage foreground;
	/**Stores the current scene stack for the game.*/
	protected Stack<Scene> sceneStack;
	
	/**Stores if the core class has been loaded or not.*/
	protected boolean loading;
	/**Stores the pixel scale for rendering.*/
	protected int pixelScale = 1;

	/**Initialises the window.*/
	@Override
	public void create() {
		/**Disables logging to speed up the program.*/
		LogManager.getLogManager().reset();
		/**Allows for native code to be loaded.*/
		GdxNativesLoader.load();
		/**Loads the VisUI library.
		 * Sets the title alignment for windows to the centre.
		 * Sets the fonts to allow colouring via the LibGDX mark-up language.*/
		VisUI.load(Gdx.files.local("assets/tixel/x1/tixel.json"));
		VisUI.setDefaultTitleAlign(Align.center);
		VisUI.getSkin().getFont("default-font").getData().markupEnabled = true;
		VisUI.getSkin().getFont("small-font").getData().markupEnabled = true;
		
		/**Loads the managers for different assets.*/
		inputManager = new InputManager();
		scriptManager = new ScriptManager();
		eventManager = new EventManager();
		textureManager = new TextureManager();
		soundManager = new SoundManager();
		textRenderer = new TextRenderer();
		/**Initialises the UserClassLoaders.*/
		sceneLoader = new UserClassLoader<Scene>(this, "scripts/scenes", Core.class, Object[].class);
		/**Initialises the scene stack.*/
		sceneStack = new Stack<Scene>();
		/**Initialises the foreground scene.*/
		foreground = new Stage(new ExtendViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		/**Begins synchronous loading by setting the flag to true.*/
		loading = true;
	}
	
	/**Disposes of any assets.*/
	@Override
	public void dispose() {
		super.dispose();
		VisUI.dispose(true);
		textureManager.dispose();
		soundManager.dispose();
		textRenderer.dispose();
		
		/**Iterates over every scene in the stack and disposes of them.*/
		while(sceneStack.isEmpty()) {
			sceneStack.pop().dispose();
		}
	}
	
	/**Runs the rendering loop.*/
	@Override
	public void render() {
		/**Handles the input manager.*/
		inputManager.tick();
		
		if(!loading) {
			/**If there is a scene currently active, renders and deals with its logic prior to any events.*/
			if(!sceneStack.isEmpty()) {
				/**Clears the screen from the previous scene.*/
				Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
				/**Gets the current scene.*/
				Scene scene = sceneStack.peek();
				/**Sets the current scene to be the input controller.*/
				Gdx.input.setInputProcessor(scene);
				/**Renders the current scene.*/
				scene.draw();
				/**Ticks the current scene.*/
				scene.tick();
			} else {
				/**Resets the input controller to null.*/
				Gdx.input.setInputProcessor(null);
			}
			
			/**Checks if the foreground scene is active.*/
			if(foreground.getActors().size > 0) {
				/**Sets the foreground scene to the the input controller.*/
				Gdx.input.setInputProcessor(foreground);
				/**Draws the foreground scene.*/
				foreground.draw();
				/**Ticks the foreground scene.*/
				foreground.act();
			}
		} else {
			/**Runs the loading script.
			 * If loading has concluded, the script will return false.*/
			loading = (Boolean) scriptManager.run("init", this);
		}
		
		/**Takes a screenshot if the F10 key was just pressed.*/
		if(inputManager.keyJustPressed(Keys.F10)) {
			screenshot();
		}
	}
	
	/**Deals with resizing the current scene and foreground stage.*/
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		/**Updates the foreground's viewport to match the width and height of the window.*/
		foreground.getViewport().update(width, height);
		
		/**Checks if the scene stack is currently populated.*/
		if(!sceneStack.isEmpty()) {
			/**Updates the current scene's viewport to match the width and height of the window.*/
			sceneStack.peek().getViewport().update(width, height);
		}
	}
	
	/**Takes a screenshot of the screen.
	 * @throws IOException */
	public void screenshot() {
		byte[] pixels = ScreenUtils.getFrameBufferPixels(0, 0,
				Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight(), true);
		/**This loop makes sure the whole screenshot is opaque and looks exactly like what the user is seeing*/
		for(int i = 4; i < pixels.length; i += 4)
		    pixels[i - 1] = (byte) 255;
		Pixmap pixmap = new Pixmap(Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight(), Pixmap.Format.RGBA8888);
		BufferUtils.copy(pixels, 0, pixmap.getPixels(), pixels.length);
		/**Creates the folder if non-existent.*/
		if(!new File("screenshots").exists())
			new File("screenshots").mkdir();
		PixmapIO.writePNG(Gdx.files.local("screenshots/screenshot "
				+ new SimpleDateFormat("dd-MM-yyyy HH mm ss").format(new Date()) + ".png"), pixmap);
		pixmap.dispose();
	}
	
	/**Shows an error dialog box to the user, shows a Scene2D box.*/
	public void showErrorDialog(String message, Throwable exception) {
		System.err.println(message);
		exception.printStackTrace();
		Dialogs.showErrorDialog(foreground, message, exception);
	}
	
	/**Gets the pixel scale.*/
	public int getPixelScale() {
		return pixelScale;
	}
	
	/**Gets the input manager.*/
	public InputManager getInputManager() {
		return inputManager;
	}
	
	/**Gets the script manager.*/
	public ScriptManager getScriptManager() {
		return scriptManager;
	}
	
	/**Gets the event manager.*/
	public EventManager getEventManager() {
		return eventManager;
	}
	
	/**Gets the texture manager.*/
	public TextureManager getTextureManager() {
		return textureManager;
	}
	
	/**Gets the sound manager.*/
	public SoundManager getSoundManager() {
		return soundManager;
	}
	
	/**Gets the Scene loader.*/
	public UserClassLoader<Scene> getSceneLoader() {
		return sceneLoader;
	}
	
	/**Gets the text renderer.*/
	public TextRenderer getTextRenderer() {
		return textRenderer;
	}
	
	/**Gets the current scene stack.*/
	public Stack<Scene> getSceneStack() {
		return sceneStack;
	}
	
	/**Gets the current scene from the stack.*/
	public Scene getScene() {
		return sceneStack.peek();
	}
	
	/**Sets the pixel scale.*/
	public void setPixelScale(int pixelScale) {
		this.pixelScale = pixelScale;
	}
	
	/**Adds a new scene to the stack.*/
	public void setScene(Scene scene) {
		sceneStack.push(scene);
	}
	
	/**Pops the current scene from the scene stack.*/
	public Scene popScene() {
		if(!sceneStack.isEmpty()) {
			return sceneStack.pop();
		} else {
			return null;
		}
	}
	
	/**Adds a new scene to the stack.
	 * Takes the scene by name.*/
	public void setScene(String sceneName, Object... parameters) {
		Scene scene = sceneLoader.load(sceneName, this, parameters);
		
		/**Checks if the scene is valid.*/
		if(scene != null) {
			this.setScene(scene);
		}
	}
}