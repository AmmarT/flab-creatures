package hts.aiproject.file;

import java.lang.reflect.ParameterizedType;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

import hts.aiproject.Core;
import hts.aiproject.utils.Loadable;

public class JsonObjectLoader<T> {
	
	/**Stores an instance of the Core class.*/
	protected Core core;
	/**Stores the JSON parser object.*/
	protected Json json;

	/**Initialises the JSON parser object.*/
	public JsonObjectLoader() {
		json = new Json(OutputType.json);
		json.setIgnoreUnknownFields(true);
	}
	
	/**Loads an object from a file of the given type. <br>
	 * Reference: {@link http://blog.xebia.com/acessing-generic-types-at-runtime-in-java/}*/
	@SuppressWarnings("unchecked")
	public T load(String path, Object... parameters) {
		T object = (T) json.fromJson(((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0]),
					new FileHandler(path).getContents());
		if(object instanceof Loadable)
			((Loadable) object).load(core, parameters);
		return object;
	}
}